# Subtractive Synth

A basic subtractive synth implemented as a learning exercise for an introduction
to DSP.

The synth uses the Synthesizer class from the JUCE framework as a base,
configured to be monophonic to keep it simple. The voice implements the
rendering of the audio and consists of a single oscilator routed through a
single filter then applified with an ADSR envelope.

### Why?
Personally I'm rather passionate about music and programming, so this project
was an oportunity for me to combine these passions and to learn about digital
signal processing. This has been an enjoyable project for me as it lets me
'hear' what I'm working with.

## Building
### Dependencies
Requires the [JUCE framework](https://juce.com/) and an IDE. This project has only been built with
XCode9 on MacOS, though in theory it should be supported by any platform
targeted by the JUCE framework (windows, mac and linux).

### Compiling
Open the `.jucer` project file with the [ProJucer](https://juce.com/discover/projucer) then click the "Save and open
in IDE" button. From there the project should be compilable from your IDE, note
that there will be multiple targets available to build: VST, VST3, AU (mac
only), a stand-alone application plus a target to build the shared library. Note
that once built, all but the stand-alone version will need to be installed to
the appropriate directory for the type of plugin.

## Oscilator

The oscilator supports 4 wave types as well as producing random noise: sine -
saw - square - triangle It is implemented using phase accumulation which allows
the oscilator to be extended with any wave function.

A naive implementation was written first using exact sqaure and saw functions.
As waves, these functions contain many harmonics, some of which may be above the
nyquist frequency leading to audio artifacts. A bandlimited oscillator was later
added which adds some 'wobbles' to the transient parts of the square and saw
functions which removes some of the higher frequency components of the signal.
This has been done using the PolyBLEP function which is a performance optimised
approximation. This function is only quasi-bandlimited meaning it improves the
sound but does not achieve perfect quality. This approach was chosen because it
provided the best trade-off between signal quality and low-latency performance
in the audio rendering pipeline.

## Filter

The filter is a basic Infinite Impulse Response filter. I took a lot of
inspiration (and coefficients) from other implementations here because filter
design is a very complex feild.

Ideally I would like to replace this with a filter modeled to replicate old
hardware such as a Moog but that is a large task and goes beyond my current
understanding of digital signal filters.

## Envelope

The envelope generator is a standard linear Attack-Decay-Sustain-Release and is
applied immediately to the amplitude. Since envelopes are time-sensitive, the
values and state is updated per sample. The envelope is also responsible for
signalling when the voice stops rendering audio which happens after the note is
released, and the sustain portion of the envelope has played out. This has been
done using a lightweight "signals & slots" implementation.

Future plans for this involve refactoring the envelope generator to purely
generate envelope signals, then re-implement the amplitude envelope as a class
that utilises this. This would allow the generator to be re-used and applied to
other parameters such as the filter, or to be applied to any parameter
dynamically.

## UI

A simple UI has been created using the JUCE UI framework. There is a discrete
5-point knob to select the wave type, 2 knobs for the filter - cutoff and
resonance, A knob for each of the 4 envelope parameters and a master volume
knob.

There is also a JUCE keyboard component which can be used to play the synth with
the keyboard or mouse.

![A basic UI built using JUCE components](SubtractiveSynthUI.png?raw=true
"Subtractive Synth UI")