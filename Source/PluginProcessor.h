/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "SubtractiveSynthAudioVoice.h"

//==============================================================================
/**
*/
class SubtractiveSynthAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    SubtractiveSynthAudioProcessor();
    ~SubtractiveSynthAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
	
	//==============================================================================
	void setOscillatorMode( dsp::OscillatorMode mode );
	void setFilterCutoff( double cutoff );
	void setFilterResonance( double resonance );
	void setEnvelopeStageValue( dsp::EnvelopeGenerator::EnvelopeStage stage, double value );
	void setMasterVolume( double volume );
	
	//==============================================================================
	// Key Board State
	MidiKeyboardState keyboardState;
	
private:
	
	SubtractiveSynthVoice& getVoice();
	
	// Synth State
	Synthesiser mSynth;
	int mSamplesPerBlock;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SubtractiveSynthAudioProcessor)
};
