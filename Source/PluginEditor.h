/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"

//==============================================================================
/**
*/
class SubtractiveSynthAudioProcessorEditor  : public AudioProcessorEditor,
											  public Slider::Listener
{
public:
    SubtractiveSynthAudioProcessorEditor (SubtractiveSynthAudioProcessor&);
    ~SubtractiveSynthAudioProcessorEditor();
	
	//==============================================================================
	// Slider::Listener Implementation
	void sliderValueChanged (Slider* slider) override;

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
	void ConnectToData();
	
	SubtractiveSynthAudioProcessor& mProcessor;
	
	// On screen keyboard
	MidiKeyboardComponent mKeyboardComponent;
	
	// OSC Knob
	Slider mOscWaveKnob;
	Label mOscWaveLabel;
	
	// Filter Knobs
	Slider mCuttoffKnob;
	Label mCuttoffLabel;
	Slider mResonanceKnob;
	Label mResonanceLabel;
	
	// Envelope Knobs
	Slider mAttackKnob;
	Label mAttackLabel;
	Slider mDecayKnob;
	Label mDecayLabel;
	Slider mSustainKnob;
	Label mSustainLabel;
	Slider mReleaseKnob;
	Label mReleaseLabel;
	
	// Master Volume Knob
	Slider mMasterVolumeKnob;
	Label mMasterVolumeLabel;
	
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SubtractiveSynthAudioProcessorEditor)
};
