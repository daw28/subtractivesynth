/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
SubtractiveSynthAudioProcessor::SubtractiveSynthAudioProcessor()
     : AudioProcessor (BusesProperties().withOutput ("Output", AudioChannelSet::stereo(), true) )
{
	mSynth.addVoice (new SubtractiveSynthVoice() );
	mSynth.addSound (new SubtractiveSynthSound() );
}

SubtractiveSynthAudioProcessor::~SubtractiveSynthAudioProcessor()
{
}

//==============================================================================
const String
SubtractiveSynthAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool
SubtractiveSynthAudioProcessor::acceptsMidi() const { return true; }

bool
SubtractiveSynthAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool
SubtractiveSynthAudioProcessor::isMidiEffect() const { return false; }

double
SubtractiveSynthAudioProcessor::getTailLengthSeconds() const { return 0.0; }

int
SubtractiveSynthAudioProcessor::getNumPrograms() { return 1; }

int
SubtractiveSynthAudioProcessor::getCurrentProgram() { return 0; }

void
SubtractiveSynthAudioProcessor::setCurrentProgram (int index) {}

const String
SubtractiveSynthAudioProcessor::getProgramName (int index) { return {}; }

void
SubtractiveSynthAudioProcessor::changeProgramName (int index, const String& newName) {}

SubtractiveSynthVoice&
SubtractiveSynthAudioProcessor::getVoice()
{
	SynthesiserVoice* voice = mSynth.getVoice(0);
	return dynamic_cast<SubtractiveSynthVoice&>(*voice);
}

void
SubtractiveSynthAudioProcessor::setOscillatorMode( dsp::OscillatorMode mode ) { getVoice().setOscillatorMode( mode ); }

void
SubtractiveSynthAudioProcessor::setFilterCutoff( double cutoff ) { getVoice().setFilterCutoff( cutoff ); }

void
SubtractiveSynthAudioProcessor::setFilterResonance( double resonance ) { getVoice().setFilterResonance( resonance ); }

void
SubtractiveSynthAudioProcessor::setEnvelopeStageValue( dsp::EnvelopeGenerator::EnvelopeStage stage, double value ) { getVoice().setEnvelopeStageValue( stage, value ); }

void
SubtractiveSynthAudioProcessor::setMasterVolume( double volume ) { getVoice().setMasterVolume( volume ); }
//==============================================================================
void
SubtractiveSynthAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
	mSamplesPerBlock = samplesPerBlock;
	mSynth.setCurrentPlaybackSampleRate (sampleRate);
	getVoice().prepareToPlay(sampleRate, samplesPerBlock);
}

void
SubtractiveSynthAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

bool
SubtractiveSynthAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{

    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     	&& layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
	{ return false; }

    return true;
}

void
SubtractiveSynthAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
	keyboardState.processNextMidiBuffer(midiMessages, 0, mSamplesPerBlock, true);
	mSynth.renderNextBlock(buffer, midiMessages, 0, buffer.getNumSamples());
	midiMessages.clear();
}

//==============================================================================
bool
SubtractiveSynthAudioProcessor::hasEditor() const { return true; }

AudioProcessorEditor*
SubtractiveSynthAudioProcessor::createEditor() { return new SubtractiveSynthAudioProcessorEditor (*this); }

//==============================================================================
void
SubtractiveSynthAudioProcessor::getStateInformation (MemoryBlock& destData)
{
}

void
SubtractiveSynthAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new SubtractiveSynthAudioProcessor();
}
