//
//  SubtractiveSynthAudioVoice.hpp
//  SubtractiveSynth
//
//  Created by David Watson on 27/07/19.
//
//

#ifndef SubtractiveSynthAudioVoice_hpp
#define SubtractiveSynthAudioVoice_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "dsp/BandlimitedOscillator.h"
#include "dsp/Mixer.h"
#include "dsp/Filter.h"
#include "dsp/EnvelopeGenerator.h"

class SubtractiveSynthSound : public SynthesiserSound
{
public:
	SubtractiveSynthSound() {}
	
	bool appliesToNote    (int) override        { return true; }
	bool appliesToChannel (int) override        { return true; }
};

class SubtractiveSynthVoice : public SynthesiserVoice
							, public dsp::EnvelopeGenerator::Listener
{
public:
	SubtractiveSynthVoice();
	~SubtractiveSynthVoice();
	
	bool canPlaySound ( SynthesiserSound* sound ) override;
	
	void startNote ( int midiNoteNumber, float velocity, SynthesiserSound*, int currentPitchWheelPosition ) override;
	void stopNote ( float velocity, bool allowTailOff ) override;
	
	void prepareToPlay( double sampleRate, int expectedNumSamples );	
	void renderNextBlock ( AudioSampleBuffer& outputBuffer, int startSample, int numSamples ) override;
	
	void pitchWheelMoved ( int newPitchWheelValue ) override;
	void controllerMoved ( int controllerNumber, int newControllerValue ) override;
	
	
	void setOscillatorMode( dsp::OscillatorMode mode );
	
	void setFilterCutoff( double cutoff );
	void setFilterResonance( double resonance );
	void setFilterMode( dsp::FilterMode mode );
	
	void setEnvelopeStageValue( dsp::EnvelopeGenerator::EnvelopeStage stage, double value );
	void envelopeReleaseFinished() override;
	
	void setMasterVolume( double volume );
	
private:
	void calculateFrequency();
	
	dsp::BandlimitedOscillator mOsc;
	dsp::Filter mFilter;
	dsp::EnvelopeGenerator mEnvelopeGenerator;
	dsp::Fader mMasterVolume;
	
	int mMidiNoteNumber;
	bool mIsPlaying;
	double mPitchBend = 0.0;
	std::vector<float> mSampleBuffer;
};

#endif /* SubtractiveSynthAudioVoice_hpp */
