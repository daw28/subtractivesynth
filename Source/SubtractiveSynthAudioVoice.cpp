//
//  SubtractiveSynthAudioVoice.cpp
//  SubtractiveSynth
//
//  Created by David Watson on 27/07/19.
//
//

#include "SubtractiveSynthAudioVoice.h"

#include <cassert>

// TODO: Implement tuning.
const double FREQUENCY_OF_A = 440.0;
// Number of notes to bend when pitch wheel is fully moved.
const double PITCH_WHEEL_MAX_NOTES = 2.0;
const double PITCH_WHEEL_MAX_POSITION = 16383.0;

SubtractiveSynthVoice::SubtractiveSynthVoice()
	: mOsc( getSampleRate() )
	, mMasterVolume( { 0.8 } )
	, mMidiNoteNumber( -1 )
	, mIsPlaying( false )
{}

SubtractiveSynthVoice::~SubtractiveSynthVoice()
{}

void
SubtractiveSynthVoice::setMasterVolume( double volume ) { mMasterVolume.setChannelVolume( 0, volume ); }

void
SubtractiveSynthVoice::setOscillatorMode(dsp::OscillatorMode mode) { mOsc.setMode( mode ); }

void
SubtractiveSynthVoice::setFilterCutoff( double cutoff ) { mFilter.setCutoff( cutoff ); }

void
SubtractiveSynthVoice::setFilterResonance( double resonance ) { mFilter.setResonance( resonance ); }

void
SubtractiveSynthVoice::setFilterMode( dsp::FilterMode mode ) { mFilter.setFilterMode( mode ); }

void
SubtractiveSynthVoice::setEnvelopeStageValue( dsp::EnvelopeGenerator::EnvelopeStage stage, double value ) { mEnvelopeGenerator.setStageValue( stage, value ); }

void
SubtractiveSynthVoice::calculateFrequency()
{
	double midiNoteFrequency = mMidiNoteNumber + mPitchBend;
	double frequency = FREQUENCY_OF_A * std::pow (2.0, (midiNoteFrequency - 69) / 12.0);
	mOsc.setFrequency( frequency );
}

void
SubtractiveSynthVoice::startNote (int midiNoteNumber, float velocity,
				SynthesiserSound*, int currentPitchWheelPosition )
{
	mIsPlaying = true;
	mMidiNoteNumber = midiNoteNumber;
	calculateFrequency();
	mEnvelopeGenerator.enterStage( dsp::EnvelopeGenerator::ENVELOPE_STAGE_ATTACK );
}

void
SubtractiveSynthVoice::envelopeReleaseFinished()
{
	mIsPlaying = false;
	mOsc.reset();
}

void
SubtractiveSynthVoice::stopNote (float velocity, bool allowTailOff)
{
	mEnvelopeGenerator.enterStage( dsp::EnvelopeGenerator::ENVELOPE_STAGE_RELEASE );
}

void
SubtractiveSynthVoice::pitchWheelMoved (int newPitchWheelValue)
{
	mPitchBend = 2 * PITCH_WHEEL_MAX_NOTES * (( newPitchWheelValue / PITCH_WHEEL_MAX_POSITION ) - 0.5);
	calculateFrequency();
}
void
SubtractiveSynthVoice::controllerMoved (int controllerNumber, int newControllerValue)
{
	// TODO: implement parameter mapping
}

bool
SubtractiveSynthVoice::canPlaySound (SynthesiserSound* sound)
{
	return dynamic_cast<SubtractiveSynthSound*> (sound) != nullptr;
}

void
SubtractiveSynthVoice::prepareToPlay(double sampleRate, int expectedNumSamples)
{
	mOsc.setSampleRate( sampleRate );
	mEnvelopeGenerator.setSampleRate( sampleRate );
	
	mSampleBuffer.resize( expectedNumSamples );
}

void
SubtractiveSynthVoice::renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples)
{
	if( mIsPlaying ) {
		assert( mSampleBuffer.size() >= numSamples );
		assert( outputBuffer.getNumSamples() >= startSample + numSamples );
		
		// Process audio chain
		mOsc.generate( mSampleBuffer.data(), numSamples );
		mFilter.process( mSampleBuffer.data(), numSamples );
		mEnvelopeGenerator.process( mSampleBuffer.data(), numSamples );
		
		mMasterVolume.process( numSamples, mSampleBuffer.data(), mSampleBuffer.data() );
		
		// Write samples out to all channels
		for( int i = 0; i < numSamples; i++ )
		{
			for (auto c = outputBuffer.getNumChannels(); --c >= 0;) {
				outputBuffer.addSample (c, startSample+i, mSampleBuffer[i]);
			}
		}
	}
}
