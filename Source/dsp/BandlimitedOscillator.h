/*
  ==============================================================================

    VoiceOscillator.h
    Created: 11 Aug 2019 12:40:25pm
    Author:  David Watson

  ==============================================================================
*/

#pragma once

#include "Oscillator.h"

namespace dsp {

// Oscillator class extended with audio rendering cleaned up for high frequencies.
class BandlimitedOscillator : public Oscillator {
public:
	BandlimitedOscillator( double sample_rate )
	: Oscillator( sample_rate )
	{}
	
protected:
	double poly_blep( double t );
	
	virtual void generateSquare( float* buffer, int n_framse ) override;
	virtual void generateSaw( float* buffer, int n_framse ) override;
};
}
