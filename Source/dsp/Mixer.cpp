/*
  ==============================================================================

    Mixer.cpp
    Created: 12 Aug 2019 9:45:23am
    Author:  David Watson

  ==============================================================================
*/

#include "Mixer.h"
#include <cassert>

using namespace dsp;

namespace {
	template< class OutputBufferType >
	void
	process( std::vector<double>& volumes, int num_samples, int i, OutputBufferType output )
	{
		return;
	}
	
	template< class OutputBufferType, class InputBuffer, class ...InputBuffers >
	void
	process( std::vector<double>& volumes, int num_samples, int channel, OutputBufferType* output, InputBuffer input, InputBuffers ...inputs )
	{
		for( int sample = 0; sample < num_samples; sample++ ) {
			output[sample] += volumes[channel] * input[sample];
		}
		
		process( volumes, num_samples, channel+1, output, inputs... );
	}
}

template<typename OutputBuffer, typename ...InputBuffers>
Mixer<OutputBuffer, InputBuffers...>::Mixer()
: mChannelVolumes( sizeof...( InputBuffers ), 1.0 )
{}

template<typename OutputBuffer, typename ...InputBuffers>
Mixer<OutputBuffer, InputBuffers...>::Mixer( std::vector<double> channel_volumes )
: mChannelVolumes( std::move( channel_volumes ) )
{}

template<typename OutputBuffer, typename ...InputBuffers>
void
Mixer<OutputBuffer, InputBuffers...>::process( int num_samples, OutputBuffer* output, InputBuffers* ...inputs )
{
	::process( mChannelVolumes, num_samples, 0, output, inputs... );
}

template<typename OutputBuffer, typename ...InputBuffers>
void
Mixer<OutputBuffer, InputBuffers...>::setChannelVolume( int channel, double volume )
{
	assert( channel < mChannelVolumes.size() );
	volume = std::min( 1.0, std::max( 0.0, volume ) );
	mChannelVolumes[ channel ] = volume;
}

template class dsp::Mixer<float, float>;
template class dsp::Mixer<float, float, float>;
