//
//  EnvelopeGenerator.cpp
//  SubtractiveSynth
//
//  Created by David Watson on 29/07/19.
//
//

#include "EnvelopeGenerator.h"

#include <cmath>

using namespace dsp;

EnvelopeGenerator::EnvelopeGenerator()
	: mCurrentStage(ENVELOPE_STAGE_OFF)
	, mCurrentLevel(MINIMUM_LEVEL)
	, mMultiplier(1.0)
	, mSampleRate(44100.0)
	, mCurrentSampleIndex(0)
	, mNextStageSampleIndex(0)
{
	mStageValue[ENVELOPE_STAGE_OFF] = 0.0;
	mStageValue[ENVELOPE_STAGE_ATTACK] = 0.01;
	mStageValue[ENVELOPE_STAGE_DECAY] = 0.5;
	mStageValue[ENVELOPE_STAGE_SUSTAIN] = 0.1;
	mStageValue[ENVELOPE_STAGE_RELEASE] = 1.0;
};

void
EnvelopeGenerator::setSampleRate(double newSampleRate) {
	mSampleRate = newSampleRate;
}

void
EnvelopeGenerator::calculateMultiplier(double startLevel,
											double endLevel,
											uint64 lengthInSamples)
{
	mMultiplier = 1.0 + (log(endLevel) - log(startLevel)) / (lengthInSamples);
}

void
EnvelopeGenerator::notifyListeners()
{
	for( EnvelopeGenerator::Listener* listener : mListeners )
	{
		listener->envelopeReleaseFinished();
	}
}

void
EnvelopeGenerator::enterStage(EnvelopeStage newStage) {
	if (mCurrentStage == newStage) return;
	if (mCurrentStage == ENVELOPE_STAGE_OFF) {
		// TODO: signal begining note here...
	}
	if (newStage == ENVELOPE_STAGE_OFF) {
		notifyListeners();
	}
	
	mCurrentStage = newStage;
	mCurrentSampleIndex = 0;
	if (mCurrentStage == ENVELOPE_STAGE_OFF ||
		mCurrentStage == ENVELOPE_STAGE_SUSTAIN) {
		mNextStageSampleIndex = 0;
	} else {
		mNextStageSampleIndex = mStageValue[mCurrentStage] * mSampleRate;
	}
	switch (newStage) {
		case ENVELOPE_STAGE_OFF:
			mCurrentLevel = 0.0;
			mMultiplier = 1.0;
			break;
		case ENVELOPE_STAGE_ATTACK:
			mCurrentLevel = MINIMUM_LEVEL;
			calculateMultiplier(mCurrentLevel,
								1.0,
								mNextStageSampleIndex);
			break;
		case ENVELOPE_STAGE_DECAY:
			mCurrentLevel = 1.0;
			calculateMultiplier(mCurrentLevel,
								fmax(mStageValue[ENVELOPE_STAGE_SUSTAIN], MINIMUM_LEVEL),
								mNextStageSampleIndex);
			break;
		case ENVELOPE_STAGE_SUSTAIN:
			mCurrentLevel = mStageValue[ENVELOPE_STAGE_SUSTAIN];
			mMultiplier = 1.0;
			break;
		case ENVELOPE_STAGE_RELEASE:
			// We could go from ATTACK/DECAY to RELEASE,
			// so we're not changing currentLevel here.
			calculateMultiplier(mCurrentLevel,
								MINIMUM_LEVEL,
								mNextStageSampleIndex);
			break;
		default:
			break;
	}
}

void
EnvelopeGenerator::setStageValue(EnvelopeStage stage,
									  double value) {
	mStageValue[stage] = value;
	
	if (stage == mCurrentStage) {
		// Re-calculate the multiplier and nextStageSampleIndex
		if(mCurrentStage == ENVELOPE_STAGE_ATTACK ||
		   mCurrentStage == ENVELOPE_STAGE_DECAY ||
		   mCurrentStage == ENVELOPE_STAGE_RELEASE) {
			double nextLevelValue=0;
			switch (mCurrentStage) {
				case ENVELOPE_STAGE_ATTACK:
					nextLevelValue = 1.0;
					break;
				case ENVELOPE_STAGE_DECAY:
					nextLevelValue = fmax(mStageValue[ENVELOPE_STAGE_SUSTAIN], MINIMUM_LEVEL);
					break;
				case ENVELOPE_STAGE_RELEASE:
					nextLevelValue = MINIMUM_LEVEL;
					break;
				default:
					break;
			}
			// How far the generator is into the current stage:
			double mCurrentStageProcess = (mCurrentSampleIndex + 0.0) / mNextStageSampleIndex;
			// How much of the current stage is left:
			double remainingStageProcess = 1.0 - mCurrentStageProcess;
			unsigned long long samplesUntilNextStage = remainingStageProcess * value * mSampleRate;
			mNextStageSampleIndex = mCurrentSampleIndex + samplesUntilNextStage;
			calculateMultiplier(mCurrentLevel, nextLevelValue, samplesUntilNextStage);
		} else if(mCurrentStage == ENVELOPE_STAGE_SUSTAIN) {
			mCurrentLevel = value;
		}
	}
	if (mCurrentStage == ENVELOPE_STAGE_DECAY &&
		stage == ENVELOPE_STAGE_SUSTAIN) {
		// We have to decay to a different sustain value than before.
		// Re-calculate multiplier:
		unsigned long long samplesUntilNextStage = mNextStageSampleIndex - mCurrentSampleIndex;
		calculateMultiplier(mCurrentLevel,
							fmax(mStageValue[ENVELOPE_STAGE_SUSTAIN], MINIMUM_LEVEL),
							samplesUntilNextStage);
	}
}

double
EnvelopeGenerator::nextSample() {
	if (mCurrentStage != ENVELOPE_STAGE_OFF &&
		mCurrentStage != ENVELOPE_STAGE_SUSTAIN) {
		if (mCurrentSampleIndex == mNextStageSampleIndex) {
			EnvelopeStage newStage = static_cast<EnvelopeStage>( (mCurrentStage + 1) % kNumEnvelopeStages );
			enterStage(newStage);
		}
		mCurrentLevel *= mMultiplier;
		mCurrentSampleIndex++;
	}
	return mCurrentLevel;
}

void
EnvelopeGenerator::process( float* buffer, int numSamples )
{
	for( int i = 0; i < numSamples; i ++ )
	{
		buffer[i] *= nextSample();
	}
}
