//
//  Oscilator.hpp
//  DavidSynth
//
//  Created by David Watson on 26/07/19.
//
//

#ifndef Oscillator_hpp
#define Oscillator_hpp

#include <stdio.h>
#include <cmath>

namespace dsp {
	// portable PI constants
	const double PI = 2*acos(0.0);
	const double TWO_PI = 2*2*acos(0.0);
	
	// default oscillator values
	const double DEFAULT_SAMPLE_RATE = 44100.0;
	const double DEFAULT_FREQUENCY = 440.0;
	
	enum OscillatorMode {
		OSCILLATOR_MODE_SINE,
		OSCILLATOR_MODE_SAW,
		OSCILLATOR_MODE_SQUARE,
		OSCILLATOR_MODE_TRIANGLE,
		OSCILLATOR_MODE_NOISE,
		kOSCILATOR_MODES
	};
	
	class Oscillator {
	public:
		Oscillator( double sample_rate );
		virtual ~Oscillator() = default;
		
		void setMode( OscillatorMode mode );
		void setOctaveShift( int octave_shift );
		void setFrequency( double frequency );
		
		void setSampleRate( double sample_rate );
		void generate( float* buffer, int n_frames );
		
		void setPhaseOffset( double phase_offset );
		void reset();
	
	protected:
		void generateSine( float* buffer, int n_framse );
		virtual void generateSaw( float* buffer, int n_framse );
		virtual void generateSquare( float* buffer, int n_framse );
		virtual void generateTriangle( float* buffer, int n_framse );
		virtual void generateNoise( float* buffer, int n_framse );
		
		void incrementPhase();
		/// Returns phase with offset normalised between 0 and 2*pi
		double currentPhase();
		void updateIncrement();
		
		OscillatorMode mOscillatorMode;
		
		int mOctaveShift;
		double mFrequency;
		double mSampleRate;

		double mPhaseIncrement; //< Value phase increases by each sample
		double mPhase;			//< Current state of waveform
		
		double mPhaseOffset;	//< Oscilator Phase offset relative to others.
	};
}

#endif /* Oscilator_hpp */
