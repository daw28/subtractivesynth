//
//  Oscilator.cpp
//  DavidSynth
//
//  Created by David Watson on 26/07/19.
//
//

#include "Oscillator.h"
#include "../JuceLibraryCode/JuceHeader.h"

using namespace dsp;

Oscillator::Oscillator( double sample_rate )
	: mOscillatorMode( OSCILLATOR_MODE_SINE )
	, mOctaveShift( 0 )
	, mFrequency( DEFAULT_FREQUENCY )
	, mSampleRate( sample_rate )
	, mPhase( 0.0 )
	, mPhaseOffset( 0.0 )
{
	updateIncrement();
}

void
Oscillator::setMode( OscillatorMode mode ) {
	if( mode < OscillatorMode::kOSCILATOR_MODES )
	{
		mOscillatorMode = mode;
	}
}

void
Oscillator::setOctaveShift( int octave_shift ) {
	mOctaveShift = octave_shift;
	updateIncrement();
}

void
Oscillator::setFrequency( double frequency ) {
	mFrequency = frequency;
	updateIncrement();
}

void
Oscillator::setSampleRate( double sample_rate ) {
	mSampleRate = sample_rate;
	updateIncrement();
}

void
Oscillator::setPhaseOffset( double phase_offset ) {
	mPhaseOffset = phase_offset;
}

void
Oscillator::reset() {
	mPhase = 0.0;
}

void
Oscillator::updateIncrement() {
	double shifted_frequency = mFrequency * std::pow (2.0, mOctaveShift);
	mPhaseIncrement = shifted_frequency * 2 * PI / mSampleRate;
}

void
Oscillator::generate( float* buffer, int n_frames ) {
	switch (mOscillatorMode) {
		case OSCILLATOR_MODE_SINE:
			generateSine( buffer, n_frames );
			break;
		case OSCILLATOR_MODE_SAW:
			generateSaw( buffer, n_frames );
			break;
		case OSCILLATOR_MODE_SQUARE:
			generateSquare( buffer, n_frames );
			break;
		case OSCILLATOR_MODE_TRIANGLE:
			generateTriangle( buffer, n_frames );
			break;
		case OSCILLATOR_MODE_NOISE:
			generateNoise( buffer, n_frames );
			break;
		default:
			break;
	}
}

void
Oscillator::incrementPhase() {
	mPhase += mPhaseIncrement;
	while( mPhase >= TWO_PI ) {
		mPhase -= TWO_PI;
	}
}

inline double
Oscillator::currentPhase() {
	double val = mPhase + mPhaseOffset;
	while( val >= TWO_PI ) {
		val -= TWO_PI;
	}
	while( val < 0 ) {
		val += TWO_PI;
	}
	return val;
}

void
Oscillator::generateSine( float* buffer, int n_frames ) {
	for( int i = 0; i < n_frames; i++ ) {
		buffer[i] = sin( currentPhase() );
		incrementPhase();
	}
}

void
Oscillator::generateSaw( float* buffer, int n_frames ) {
	for( int i = 0; i < n_frames; i++ ) {
		buffer[i] = 1.0 - ( 2.0 * currentPhase() / TWO_PI );
		incrementPhase();
	}
}

void
Oscillator::generateSquare( float* buffer, int n_frames ) {
	for( int i = 0; i < n_frames; i++ ) {
		if( currentPhase() <= PI ) {
			buffer[i] = 1.0;
		} else {
			buffer[i] = -1.0;
		}
		incrementPhase();
	}
}

void
Oscillator::generateTriangle( float* buffer, int n_frames ) {
	for( int i = 0; i < n_frames; i++ ) {
		if( currentPhase() <= PI ) {
			buffer[i] = -1.0 + ( 2.0 * currentPhase() / PI );
		} else {
			buffer[i] = 1.0 - ( 2.0 * ( currentPhase() + PI ) / PI );
		}
		incrementPhase();
	}
}

// Harsh Random Noise.
void
Oscillator::generateNoise( float* buffer, int n_frames ) {
	for( int i = 0; i < n_frames; i++ )
	{
		double value = juce::Random::getSystemRandom().nextInt( 2000000 ) / 1000000.0;
		buffer[i] = -1.0 + value;
	}
}


