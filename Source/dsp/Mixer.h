/*
  ==============================================================================

    Mixer.h
    Created: 11 Aug 2019 11:10:49pm
    Author:  David Watson

  ==============================================================================
*/

#pragma once

#include <vector>

namespace dsp {
	
template<typename OutputBuffer, typename ...InputBuffers>
class Mixer {
public:
	Mixer();
	Mixer( std::vector<double> channel_volumes );

	void process( int num_samples, OutputBuffer* output, InputBuffers* ...inputs );
	void setChannelVolume( int channel, double volume );

protected:
    std::vector<double> mChannelVolumes;
};

typedef Mixer<float, float> Fader;
typedef Mixer<float, float, float> TwoChannelMixer;
}
