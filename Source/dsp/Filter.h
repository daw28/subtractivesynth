//
//  Filter.hpp
//  Synth
//

#ifndef Filter_hpp
#define Filter_hpp

namespace dsp {

enum FilterMode {
    FILTER_MODE_BANDPASS = 0,
    FILTER_MODE_HIGHPASS,
    FILTER_MODE_LOWPASS,
    kNumFilterModes
};
enum FilterDB {
    FILTER_12_DB = 0,
    FILTER_24_DB,
    kNumFilterDB
};

class Filter {
public:
    Filter();
    
    double process( double inputValue );
	void process( float* inputBuffer, int numSamples );
	
    void setCutoff( double newCutoff );
    void setResonance( double resonance );
    void setFilterMode( FilterMode mode );
    void setFilterDB( FilterDB db );
    void setCutoffMod( double cutoffMod );
private:
    inline void calculateFeedbackAmount();
    inline double getCalculatedCutoff() const;

    double mCutoff;
    double mCutoffMod;
    double mResonance;
    
    FilterMode mMode;
    FilterDB mFilterDB;
    
    double mFeedbackAmount;
    double mBuf0;
    double mBuf1;
    double mBuf2;
    double mBuf3;
};
}
#endif /* Filter_hpp */
