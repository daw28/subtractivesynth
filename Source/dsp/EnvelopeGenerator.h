//
//  EnvelopeGenerator.hpp
//  SubtractiveSynth
//
//  Created by David Watson on 29/07/19.
//
//

#ifndef EnvelopeGenerator_hpp
#define EnvelopeGenerator_hpp

#include <vector>

using uint64 = unsigned long long;

namespace dsp {

class EnvelopeGenerator {
public:
	enum EnvelopeStage {
		ENVELOPE_STAGE_OFF = 0,
		ENVELOPE_STAGE_ATTACK,
		ENVELOPE_STAGE_DECAY,
		ENVELOPE_STAGE_SUSTAIN,
		ENVELOPE_STAGE_RELEASE,
		kNumEnvelopeStages
	};
	
	class Listener {
	public:
		virtual void envelopeReleaseFinished() = 0;
		virtual ~Listener() = default;
	};
	
	const double MINIMUM_LEVEL = 0.00001;
	
	EnvelopeGenerator();
	
	void enterStage(EnvelopeStage newStage);
	double nextSample();
	void process( float* buffer, int numSamples );
	
	void setSampleRate(double newSampleRate);
	void setStageValue(EnvelopeStage stage, double value);
	
	void addListener( Listener* listener ) { mListeners.push_back( listener ); }
	
	inline EnvelopeStage getCurrentStage() const { return mCurrentStage; };
	
private:
	void calculateMultiplier(double startLevel, double endLevel, uint64 lengthInSamples);
	
	// TODO: Do this in a generic fashion
	std::vector<Listener*> mListeners;
	void notifyListeners();
	
	EnvelopeStage mCurrentStage;
	double mCurrentLevel;
	double mMultiplier;
	double mSampleRate;
	double mStageValue[kNumEnvelopeStages];

	uint64 mCurrentSampleIndex;
	uint64 mNextStageSampleIndex;
};
}
#endif /* EnvelopeGenerator_hpp */
