//
//  VolumeFader.cpp
//  SubtractiveSynth
//
//  Created by David Watson on 10/08/19.
//
//

#include "Fader.h"

using namespace dsp;

Fader::Fader( double volume, double min_val, double max_val )
: mVolume( volume )
, mMinVal( min_val )
, mMaxVal( max_val )
{}

void
Fader::setVolume( double volume )
{
	mVolume = std::max( mMinVal, std::min( mMaxVal, volume ) );
}

void
Fader::process( float* inputBuffer, int numSamples )
{
	for( int i = 0; i < numSamples; i++ )
	{
		inputBuffer[i] = inputBuffer[i] * mVolume;
	}
}
