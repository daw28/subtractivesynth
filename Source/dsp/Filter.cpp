//
//  Filter.cpp
//  Synth
//

#include "Filter.h"
#include <math.h>

using namespace dsp;

Filter::Filter()
    : mCutoff(0.99)
    , mCutoffMod(0.0)
    , mResonance(0.0)
    , mMode(FILTER_MODE_LOWPASS)
    , mBuf0(0.0)
    , mBuf1(0.0)
    , mBuf2(0.0)
    , mBuf3(0.0)
{
    calculateFeedbackAmount();
};

void
Filter::calculateFeedbackAmount() {
    mFeedbackAmount = mResonance + mResonance/(1.0 - getCalculatedCutoff());
}

double
Filter::getCalculatedCutoff() const {
    return fmax(fmin(mCutoff + mCutoffMod, 0.99), 0.01);
};

void
Filter::setCutoff( double newCutoff ) { mCutoff = newCutoff; calculateFeedbackAmount(); }
void
Filter::setResonance( double resonance ) { mResonance = resonance; calculateFeedbackAmount(); }
void
Filter::setFilterMode( FilterMode mode ) { mMode = mode; }
void
Filter::setFilterDB( FilterDB db ) { mFilterDB = db; }
void
Filter::setCutoffMod( double cutoffMod ) { mCutoffMod = cutoffMod; calculateFeedbackAmount(); }

// By Paul Kellett
// http://www.musicdsp.org/showone.php?id=29
double
Filter::process(double inputValue) {
    if (inputValue == 0.0) return inputValue;
    double calculatedCutoff = getCalculatedCutoff();
    mBuf0 += calculatedCutoff * (inputValue - mBuf0 + mFeedbackAmount * (mBuf0 - mBuf1));
    mBuf1 += calculatedCutoff * (mBuf0 - mBuf1);
    mBuf2 += calculatedCutoff * (mBuf1 - mBuf2);
    mBuf3 += calculatedCutoff * (mBuf2 - mBuf3);
    
    switch (mMode) {
        case FILTER_MODE_LOWPASS:
            return mFilterDB ? mBuf1 : mBuf3;
        case FILTER_MODE_HIGHPASS:
            return inputValue - (mFilterDB ? mBuf1 : mBuf3);
        case FILTER_MODE_BANDPASS:
            return mBuf0 - mBuf3;
        default:
            return 0.0;
    }
}

void
Filter::process( float* inputBuffer, int numSamples )
{
	for( int i = 0; i < numSamples; i++ )
	{
		inputBuffer[i] = process((double)inputBuffer[i]);
	}
}
