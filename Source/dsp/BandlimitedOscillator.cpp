/*
  ==============================================================================

    VoiceOscillator.cpp
    Created: 11 Aug 2019 12:40:01pm
    Author:  David Watson

  ==============================================================================
*/

#include "BandlimitedOscillator.h"

using namespace dsp;
// from: http://metafunction.co.uk/all-about-digital-oscillators-part-2-blits-bleps/
// This function calculates the PolyBLEPs
double
BandlimitedOscillator::poly_blep(double t)
{
	const double dt = mPhaseIncrement / TWO_PI;
	
	// t-t^2/2 +1/2
	// 0 < t <= 1
	// discontinuities between 0 & 1
	if (t < dt)
	{
		t /= dt;
		return t + t - t * t - 1.0;
	}
	
	// t^2/2 +t +1/2
	// -1 <= t <= 0
	// discontinuities between -1 & 0
	else if (t > 1.0 - dt)
	{
		t = (t - 1.0) / dt;
		return t * t + t + t + 1.0;
	}
	
	// no discontinuities
	// 0 otherwise
	else return 0.0;
}

void
BandlimitedOscillator::generateSquare( float* buffer, int n_frames )
{
	for( int i = 0; i < n_frames; i++ ) {
		const double t = currentPhase() / TWO_PI; // Define half phase
	
		if (( currentPhase() ) < PI)
		{
			buffer[i] = 1.0;
		} else {
			buffer[i] = -1.0;
		}
		
		buffer[i] += poly_blep(t); // Layer output of Poly BLEP on top (flip)
		buffer[i] -= poly_blep(fmod(t + 0.5, 1.0)); // Layer output of Poly BLEP on top (flop)
		incrementPhase();
	}
}

void
BandlimitedOscillator::generateSaw(float *buffer, int n_frames )
{
	for( int i = 0; i < n_frames; i++ ) {
		const double t = currentPhase() / TWO_PI; // Define half phase
		buffer[i] = (2.0 * currentPhase() / TWO_PI) - 1.0;
		buffer[i] -= poly_blep(t); // Layer output of Poly BLEP on top
		incrementPhase();
	}
}
