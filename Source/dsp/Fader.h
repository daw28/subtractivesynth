//
//  VolumeFader.hpp
//  SubtractiveSynth
//
//  Created by David Watson on 10/08/19.
//
//

#ifndef VolumeFader_hpp
#define VolumeFader_hpp

#include <algorithm>

namespace dsp {

class Fader {
public:
	
	Fader( double volume, double min_val = 0.0, double max_val = 1.0 );
	void setVolume( double volume );
	
	void process( float* inputBuffer, int numSamples );
	
protected:
	double mVolume;
	double mMinVal;
	double mMaxVal;
};
}

#endif /* VolumeFader_hpp */
