/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

#include <cassert>

//==============================================================================
SubtractiveSynthAudioProcessorEditor::SubtractiveSynthAudioProcessorEditor (SubtractiveSynthAudioProcessor& p)
    : AudioProcessorEditor (p)
	, mProcessor (p)
	, mKeyboardComponent (p.keyboardState, MidiKeyboardComponent::horizontalKeyboard)
	, mOscWaveKnob(Slider::SliderStyle::RotaryVerticalDrag, Slider::TextEntryBoxPosition::NoTextBox)
	, mCuttoffKnob(Slider::SliderStyle::RotaryVerticalDrag, Slider::TextEntryBoxPosition::NoTextBox)
	, mResonanceKnob(Slider::SliderStyle::RotaryVerticalDrag, Slider::TextEntryBoxPosition::NoTextBox)
	, mAttackKnob(Slider::SliderStyle::RotaryVerticalDrag, Slider::TextEntryBoxPosition::NoTextBox)
	, mDecayKnob(Slider::SliderStyle::RotaryVerticalDrag, Slider::TextEntryBoxPosition::NoTextBox)
	, mSustainKnob(Slider::SliderStyle::RotaryVerticalDrag, Slider::TextEntryBoxPosition::NoTextBox)
	, mReleaseKnob(Slider::SliderStyle::RotaryVerticalDrag, Slider::TextEntryBoxPosition::NoTextBox)
	, mMasterVolumeKnob(Slider::SliderStyle::RotaryVerticalDrag, Slider::TextEntryBoxPosition::NoTextBox)
{
	// Set window size. Fized for now.
	setSize (600, 160);

	addAndMakeVisible(mKeyboardComponent);
	
	mOscWaveKnob.setRange(0.0, dsp::OscillatorMode::kOSCILATOR_MODES - 1, 1.0);
	mOscWaveKnob.setValue( dsp::OscillatorMode::OSCILLATOR_MODE_SINE );
	
	mCuttoffKnob.setRange(0, 1);		mCuttoffKnob.setValue( 0.99 );
	mResonanceKnob.setRange(0, 1.0);	mResonanceKnob.setValue(0.0);
	
	mAttackKnob.setRange(0.01, 1.0);	mAttackKnob.setValue(0.01);
	mDecayKnob.setRange(0, 1.0);		mDecayKnob.setValue(0.5);
	mSustainKnob.setRange(0, 1.0);		mSustainKnob.setValue(0.1);
	mReleaseKnob.setRange(0, 2.0);		mReleaseKnob.setValue(1.0);
	
	mMasterVolumeKnob.setRange(0.0, 1.0); mMasterVolumeKnob.setValue( 0.8 );
	
	addAndMakeVisible(mOscWaveKnob);
	addAndMakeVisible(mOscWaveLabel);
	mOscWaveLabel.setJustificationType( Justification::centredBottom );
	mOscWaveLabel.setText("Wave", dontSendNotification);
	mOscWaveLabel.attachToComponent(&mOscWaveKnob, false);
	
	addAndMakeVisible(mCuttoffKnob);
	addAndMakeVisible(mCuttoffLabel);
	mCuttoffLabel.setJustificationType( Justification::centredBottom );
	mCuttoffLabel.setText("Cut", dontSendNotification);
	mCuttoffLabel.attachToComponent(&mCuttoffKnob, false);
	
	addAndMakeVisible(mResonanceKnob);
	addAndMakeVisible(mResonanceLabel);
	mResonanceLabel.setText("Res", dontSendNotification);
	mResonanceLabel.setJustificationType( Justification::centredBottom );
	mResonanceLabel.attachToComponent(&mResonanceKnob, false);
	
	addAndMakeVisible(mAttackKnob);
	addAndMakeVisible(mAttackLabel);
	mAttackLabel.setJustificationType( Justification::centredBottom );
	mAttackLabel.setText("A", dontSendNotification);
	mAttackLabel.attachToComponent(&mAttackKnob, false);
	
	addAndMakeVisible(mDecayKnob);
	addAndMakeVisible(mDecayLabel);
	mDecayLabel.setJustificationType( Justification::centredBottom );
	mDecayLabel.setText("D", dontSendNotification);
	mDecayLabel.attachToComponent(&mDecayKnob, false);
	
	addAndMakeVisible(mSustainKnob);
	addAndMakeVisible(mSustainLabel);
	mSustainLabel.setJustificationType( Justification::centredBottom );
	mSustainLabel.setText("S", dontSendNotification);
	mSustainLabel.attachToComponent(&mSustainKnob, false);
	
	addAndMakeVisible(mReleaseKnob);
	addAndMakeVisible(mReleaseLabel);
	mReleaseLabel.setJustificationType( Justification::centredBottom );
	mReleaseLabel.setText("R", dontSendNotification);
	mReleaseLabel.attachToComponent(&mReleaseKnob, false);
	
	addAndMakeVisible(mMasterVolumeKnob);
	addAndMakeVisible(mMasterVolumeLabel);
	mMasterVolumeLabel.setJustificationType( Justification::centredBottom );
	mMasterVolumeLabel.setText("Vol", dontSendNotification);
	mMasterVolumeLabel.attachToComponent(&mMasterVolumeKnob, false);
	
	ConnectToData();
}

void
SubtractiveSynthAudioProcessorEditor::ConnectToData()
{
	mOscWaveKnob.addListener(this);
	
	mCuttoffKnob.addListener(this);
	mResonanceKnob.addListener(this);

	mAttackKnob.addListener(this);
	mDecayKnob.addListener(this);
	mSustainKnob.addListener(this);
	mReleaseKnob.addListener(this);
	
	mMasterVolumeKnob.addListener(this);
}

void SubtractiveSynthAudioProcessorEditor::sliderValueChanged(Slider* slider)
{
	auto value = slider->getValueObject().getValue();
	if( value.isDouble() )
	{
		if( slider == &mOscWaveKnob )
		{
			mProcessor.setOscillatorMode( (dsp::OscillatorMode)(int)value );
		}
		else if( slider == &mCuttoffKnob )
		{
			mProcessor.setFilterCutoff( value );
		}
		else if( slider == &mResonanceKnob )
		{
			mProcessor.setFilterResonance( value );
		}
		else if( slider == &mAttackKnob )
		{
			mProcessor.setEnvelopeStageValue(dsp::EnvelopeGenerator::EnvelopeStage::ENVELOPE_STAGE_ATTACK, value);
		}
		else if( slider == &mDecayKnob )
		{
			mProcessor.setEnvelopeStageValue(dsp::EnvelopeGenerator::EnvelopeStage::ENVELOPE_STAGE_DECAY, value);
		}
		else if( slider == &mSustainKnob )
		{
			mProcessor.setEnvelopeStageValue(dsp::EnvelopeGenerator::EnvelopeStage::ENVELOPE_STAGE_SUSTAIN, value);
		}
		else if( slider == &mReleaseKnob )
		{
			mProcessor.setEnvelopeStageValue(dsp::EnvelopeGenerator::EnvelopeStage::ENVELOPE_STAGE_RELEASE, value);
		}
		else if( slider == &mMasterVolumeKnob )
		{
			mProcessor.setMasterVolume( value );
		}
		else
		{
			assert(false);
		}
	}
	

}

SubtractiveSynthAudioProcessorEditor::~SubtractiveSynthAudioProcessorEditor()
{
}

//==============================================================================
void
SubtractiveSynthAudioProcessorEditor::paint (Graphics& g)
{
	g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void
SubtractiveSynthAudioProcessorEditor::resized()
{
	mKeyboardComponent.setBounds (8, 96, getWidth() - 16, 64);
	
	mOscWaveKnob.setBounds(8, 32, 40, 40);
	
	mCuttoffKnob.setBounds(64, 32, 40, 40);
	mResonanceKnob.setBounds(112, 32, 40, 40);
	
	mAttackKnob.setBounds(168, 32, 40, 40);
	mDecayKnob.setBounds(216, 32, 40, 40);
	mSustainKnob.setBounds(264, 32, 40, 40);
	mReleaseKnob.setBounds(312, 32, 40, 40);
	
	mMasterVolumeKnob.setBounds(368, 32, 40, 40);

}
